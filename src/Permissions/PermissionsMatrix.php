<?php

namespace Drupal\permatrix\Permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\Yaml\Yaml;

class PermissionsMatrix implements ContainerInjectionInterface {
  
  protected $configFactory;

  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  public function permissions() {
    $config = $this->configFactory->get('permatrix.permission_matrix.config');
    $permissions = $config->get();
    return $permissions;
  }
}