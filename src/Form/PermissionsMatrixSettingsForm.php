<?php

namespace Drupal\permatrix\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Yaml;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;

/**
 * Permission Matrix Settings Form.
 */
class PermissionsMatrixSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  protected $messenger;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $moduleHandler,
    MessengerInterface $messenger) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'permatrix.permission_matrix.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permatrix_permissions_matrix';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('permatrix.permission_matrix.config');
    $conf = $config->get();
    $config_text = Yaml::encode($conf);

    if (!$this->moduleHandler->moduleExists('yaml_editor')) {
      $message = $this->t('It is recommended to install the <a href="@yaml-editor">YAML Editor</a> module for easier editing.', [
        '@yaml-editor' => 'https://www.drupal.org/project/yaml_editor',
      ]);
      $this->messenger->addMessage($message, 'warning');
    }

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Permission Matrix Configuration'),
      '#description' => $this->t('Define permission in YAML format.'),
      '#default_value' => $config_text,
      '#rows' => 15,
      '#attributes' => ['data-yaml-editor' => 'true'],
    ];

    // Use module's YAML config file for example structure.
    $module_path = $this->moduleHandler->getModule('permatrix')->getPath();
    $yml_text = file_get_contents($module_path . '/config/example/permission.matrix.yml');

    $form['example'] = [
      '#type' => 'details',
      '#title' => $this->t('Example structure'),
    ];

    $form['example']['description'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Each attribute has an optional description.'),
    ];

    $form['example']['code'] = [
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
      '#markup' => $yml_text,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config_text = $form_state->getValue('config') ? $form_state->getValue('config') : '';
    try {
      $parse = SymfonyYaml::parse($config_text);
      if (!empty(Yaml::decode($config_text))) {
        $form_state->set('config', Yaml::decode($config_text));
      }
      else {
        throw new \Exception('Permission matrix cannot be empty');
      }
    }
    catch (ParseException $e) {
      $form_state->setErrorByName('config', $e->getMessage());
    }
    catch (InvalidDataTypeException $e) {
      $form_state->setErrorByName('config', $e->getMessage());
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('config', $e->getMessage());
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $form_state->get('config');
    $this->config('permatrix.permission_matrix.config')
      ->setData($config)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
